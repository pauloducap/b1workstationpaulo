Abeille Paul-Antoine

# TP1

----------------------
## HOST OS

----------------------------
````PS C:\Users\Paula> hostname````
`LAPTOP-0KNOIJE1`

````PS C:\Users\Paula> systeminfo````

``Microsoft Windows 10 Famille
10.0.19041 N/A version 19041
x64-based PC
Mémoire physique totale:                    16 209 Mo``


 ``PS C:\Users\Paula> wmic MemoryChip get Capacity``
 
`` Capacity
8589934592
8589934592 ``

````PS C:\Users\Paula> wmic MemoryChip get manufacturer````

``Manufacturer
Samsung
Samsung``

````PS C:\Users\Paula> wmic memorychip get ConfiguredClockSpeed````

``ConfiguredClockSpeed
3200
3200``

--------------------------------
## DEVICES

---
````PS C:\Users\Paula> wmic cpu get name````
``Name
Intel(R) Core(TM) i7-10875H CPU @ 2.30GHz``
``Le h signifie que ce processeur est de  Haute performance et qu'il est optimisé pour pc portable.
i7 désigne la série à laquelle il fait partie.
Le 10 représente la génération de ce proc.
la fréquence de base est cadencée à 2.30GHz
c'est un proc Intel.``

``PS C:\Users\Paula> wmic cpu get threadcount``
``ThreadCount
16 ``
``soit 8 coeurs et 16 processeurs logiques``

``Mon TouchPad: (je n'ai pas réussi à trouver la commande, je suis donc aller dans Informatiosn système)
ID de périphérique Plug-and-Play	HID\MSFT0001&COL01\5&374265B2&1&0000``

``PS C:\Users\Paula> Get-WmiObject win32_VideoController | Format-List Name``
``Name : NVIDIA GeForce RTX 2070 Super with Max-Q Design
Name : Intel(R) UHD Graphics``

``PS C:\Users\Paula> get-physicaldisk``
``media type- - 
SSD
manufacturer
Western Digital 
WDC PC SN730 SDBPNTY-1T00-1101``

``PS C:\Users\Paula> get-disk``
``total size
953.87 GB``

``PS C:\Users\Paula> get-partition``

````
PartitionNumber  DriveLetter  Size Type
---------------  -----------  ---- ----
1      | 260 MB |System
2  |   16 MB| Reserved
3 | C  | 952.62 GB |Basic
4     |  1000 MB| Recovery
````
``PS C:\Users\Paula> get-volume``

````
DriveLetter FriendlyName FileSystemType DriveType HealthStatus OperationalStatus SizeRemaining      Size
----------- ------------ -------------- --------- ------------ ----------------- -------------      ----
            WINRE_DRV    NTFS           Fixed     Healthy      OK                    345.57 MB   1000 MB
C           Windows-SSD  NTFS           Fixed     Healthy      OK                    206.27 GB 952.62 GB
````
``La partition 1 est nécessaire pour démarrer l'ordinateur avec windows.
La partition 2 est la même que la 1.
La partition 3 représente la partition de base là où tout sera installé y compris windows.
La partition 4 aide à restaurer les paramètres d'usine au cas où il y aurai une erreur système.``

----------------------
## Users

----------------------------

``PS C:\Users\Paula> net user``

````
comptes d’utilisateurs de \\LAPTOP-0KNOIJE1

-------------------------------------------------------------------------------
Administrateur           DefaultAccount           Invité
Paula                    WDAGUtilityAccount
````
----------------------
## Processus

----------------------------

``PS C:\Users\Paula> get-process``

``svchost : Permet aux utilisateurs à distance de modifier les paramètres du Registre sur cet ordinateur.``

``services : Active les messages d’événements émis par les programmes fonctionnant sous Windows et les composants devant être affichés dans l’observateur d’événements. Ce service ne peut être arrêté.``

``vssvc : Gère et implémente les clichés instantanés de volumes pour les sauvegardes et autres utilisations. Si ce service est arrêté, les clichés instantanés ne seront pas disponibles pour la sauvegarde et celle-ci échouera.``

``SCardSvr : Gère l’accès aux cartes à puce lues par cet ordinateur. Si ce service est arrêté, cet ordinateur ne pourra plus lire de cartes à puces. Si ce service est désactivé, tout service en dépendant explicitement ne démarrera pas.``

``wmiapsrv :  Fournit des informations concernant la bibliothèque de performance à partir des fournisseurs HiPerf WMI.``

----------------------
## Network

----------------------------

``PS C:\WINDOWS\system32> systeminfo``

````
Carte(s) réseau:                            2 carte(s) réseau installée(s).
                                            [01]: Intel(R) Wi-Fi 6 AX201 160MHz
                                                  Nom de la connexion : Wi-Fi
                                                  DHCP activé :         Oui
                                                  Serveur DHCP :        192.168.1.1
                                                  Adresse(s) IP
                                                  [01]: 192.168.1.50
                                                  [02]: fe80::50ea:ec35:890f:4042
                                                  [03]: 2a01:cb19:43e:300:8061:9938:3ca6:d04c
                                                  [04]: 2a01:cb19:43e:300:50ea:ec35:890f:4042
                                            [02]: Realtek PCIe GbE Family Controller
                                                  Nom de la connexion : Ethernet
                                                  État :                Support déconnecté

````
``La première carte réseau [01] représente celle du WIFI donc la connexion par ondes éléctromagnétiques.``
``La deuxième carte réseau [02] représente celle de la connexion ETHERNET autrement dit par cable.``

``PS C:\WINDOWS\system32> netstat -b``

`````
Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    127.0.0.1:49746        LAPTOP-0KNOIJE1:56695  ESTABLISHED
 [NVIDIA Web Helper.exe]
  TCP    127.0.0.1:51028        LAPTOP-0KNOIJE1:51027  TIME_WAIT
  TCP    127.0.0.1:51062        LAPTOP-0KNOIJE1:51064  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:51062        LAPTOP-0KNOIJE1:51065  ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:51064        LAPTOP-0KNOIJE1:51062  ESTABLISHED
 [Code.exe]
  TCP    127.0.0.1:51065        LAPTOP-0KNOIJE1:51062  ESTABLISHED
 [Code.exe]
  TCP    127.0.0.1:51483        LAPTOP-0KNOIJE1:64606  TIME_WAIT
  TCP    127.0.0.1:56652        LAPTOP-0KNOIJE1:65001  ESTABLISHED
 [nvcontainer.exe]
  TCP    127.0.0.1:56695        LAPTOP-0KNOIJE1:49746  ESTABLISHED
 [NVIDIA Share.exe]
  TCP    127.0.0.1:64585        LAPTOP-0KNOIJE1:64584  TIME_WAIT
  TCP    127.0.0.1:64599        LAPTOP-0KNOIJE1:64598  TIME_WAIT
  TCP    127.0.0.1:64603        LAPTOP-0KNOIJE1:64602  TIME_WAIT
  TCP    127.0.0.1:64647        LAPTOP-0KNOIJE1:64606  TIME_WAIT
  TCP    127.0.0.1:64649        LAPTOP-0KNOIJE1:64677  TIME_WAIT
  TCP    127.0.0.1:64649        LAPTOP-0KNOIJE1:64877  TIME_WAIT
  TCP    127.0.0.1:64854        LAPTOP-0KNOIJE1:64649  TIME_WAIT
  TCP    127.0.0.1:65001        LAPTOP-0KNOIJE1:56652  ESTABLISHED
 [nvcontainer.exe]
  TCP    192.168.1.50:49701     162.159.137.232:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.50:50982     69.173.144.143:https   TIME_WAIT
  TCP    192.168.1.50:51466     162.159.134.232:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.50:51500     151.101.122.214:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51507     151.101.122.214:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51509     ec2-54-202-160-96:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51511     ec2-18-236-30-18:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51515     151.101.122.167:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51526     151.101.122.167:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51527     151.101.122.167:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51556     151.101.122.133:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51626     151.101.120.176:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51920     151.101.122.133:https  ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51921     53:https               ESTABLISHED
 [chrome.exe]
  TCP    192.168.1.50:51922     229:https              TIME_WAIT
  TCP    192.168.1.50:51926     6:https                TIME_WAIT
  TCP    192.168.1.50:51949     162.159.130.234:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.50:51950     162.159.128.233:https  TIME_WAIT
  TCP    192.168.1.50:51952     162.159.137.234:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.50:51953     162.159.128.235:https  ESTABLISHED
 [Discord.exe]
  TCP    192.168.1.50:51970     52.113.199.116:https   ESTABLISHED
 [Teams.exe]
  TCP    192.168.1.50:52000     151.101.129.21:https   ESTABLISHED

`````
Je n'ai pas mis tous mes ports en utilisation car j'en ai beaucoup trop.

``[NVIDIA Web Helper.exe]Le processus écoute ou envoie des données sur des ports ouverts à un réseau local ou à Internet.``

``[Code.exe]code.exe est un fichier exécutable sur le disque dur de votre ordinateur. Il contient un code machine. Quand vous démarrez le logiciel code sur votre PC, les commandes contenues dans code.exe seront exécutées sur votre PC. Dans ce but, le fichier est chargé dans la mémoire principale (RAM) et y fonctionne comme un processus code (aussi appelé tâche).
``
``[nvcontainer.exe]Le processus NVIDIA Container appartient au logiciel NVIDIA Container ou NVIDIA LocalSystem Container ou NVIDIA Drivers ou NVIDIA Display Control Panel de la compagnie NVIDIA``

``[NVIDIA Share.exe]Les processus NVIDIA Share semblent faire partie de la superposition GeForce Experience.``


``[discord.exe]Discord est un programme VoIP multiplate-forme gratuit et populaire auprès des communautés de jeux en ligne. Discord.exe exécute Discord.``

``[chrome.exe]Le fichier authentique chrome.exe est un composant logiciel de Google Chrome de Google .
Chrome.exe est un fichier exécutable qui exécute le navigateur Web Google Chrome, un logiciel gratuit qui affiche des pages Web.``

``[Teams.exe]Le processus "Teams.exe" représente le programme "Microsoft Teams", qui est un programme client pour le service cloud "Microsoft Teams". Le processus "Teams.exe" est créé par le programme de démarrage: "Microsoft Teams".``


----------------------
## Scripting

----------------------------

auteur: abeille paul antoine
date:01/11/2020
le script affiche le nom du pc , l'IP,OS et sa version, la date et l'heure d'allumage,nous indique si l'OS est à jour, espace disque utilisé et l'espace disque dispo.
hostname
"IP"
"---------------"
ipconfig | ? { $_ -match 'Ipv4' }

"OS"
"----------------"
$os = Get-WmiObject -Class Win32_OperatingSystem
$os.Caption
$os.Version


$time = Get-CimInstance -ClassName win32_operatingsystem | select lastbootuptime
Write-Host -fore red $time
"STOCKAGE"
"------------"
$fields = "DeviceID",@{label = "espace max (GB)"; Expression = {[math]::round($_.Size / 1gb,2)}},@{label = "espace libre (GB)"; Expression = {[math]::round($_.FreeSpace / 1gb,2)}},@{label = "% Occupation"; Expression = {[math]::round(($_.Size-$_.FreeSpace) * 100 / $_.Size,2)}}
Get-WmiObject -Class Win32_LogicalDisk -Filter "DriveType=3" | format-table $fields | Out-String

"MISE A JOUR ?"
"-----------------"

$Isuptodate = "Votre OS utilise la dernière version connue"
$Isntuptodate = "Votre OS n'utilise pas la dernière version."
$updatecount = "Le nombre d'updates a faire sur l'OS est de:"
$updateObject = New-Object -ComObject Microsoft.Update.Session
$updateObject.ClientApplicationID = "Serverfault Example Script"
$updateSearcher = $updateObject.CreateUpdateSearcher()
$searchResults = $updateSearcher.Search("IsInstalled=0")
if ( $searchResults.Updates.Count -eq 0 ) {
Write-Host $Isuptodate
} else { 
Write-Host $Isntuptodate $updatecount $searchResults.Updates.Count
}


"TEST CONNECTION"
"------------"
ping 8.8.8.8
"USERS"
"-------------"
get-localuser | select Name



----------------------
## Gestion de softs

----------------------------

Un gestionnaire de paquets est un  outil automatisant le processus d'installation, désinstallation, mise à jour de logiciels installés sur un système informatique.
Par rapport au téléchargement en direct sur internet , le gestionnaire de paquets est beaucoup plus sécurisé.
Les paquets seront fiables et permettent d'avoir un fichier dans son intégralité.


j'ai actuellement 5674 packages donc je ne vais pas tous les lister.


---------------------
## Machine virtuelle

----------------------------
J'ai installé SAMBA, 
j'ai ensuite configuré le partage de fichier  en créant un fichier sur mon bureau que j'ai nommé share et j'ai redémarrer mon pc .Suite à cela , je me suis connécté de la vm à mon pc et je peux ainsi gérer à distance ce fichier.

Le partage de fichier est désormais activé.

![](https://i.imgur.com/DurTbYo.png)
