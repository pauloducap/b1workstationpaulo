"auteur: abeille paul antoine"
"date:01/11/2020"
"le script affiche le nom du pc , l'IP,OS et sa version, la date et l'heure d'allumage,nous indique si l'OS est à jour, espace disque utilisé et l'espace disque dispo."
hostname
"IP"
"---------------"
ipconfig | ? { $_ -match 'Ipv4' }

"OS"
"----------------"
$os = Get-WmiObject -Class Win32_OperatingSystem
$os.Caption
$os.Version


$time = Get-CimInstance -ClassName win32_operatingsystem | select lastbootuptime
Write-Host -fore red $time
"STOCKAGE"
"------------"
$fields = "DeviceID",@{label = "espace max (GB)"; Expression = {[math]::round($_.Size / 1gb,2)}},@{label = "espace libre (GB)"; Expression = {[math]::round($_.FreeSpace / 1gb,2)}},@{label = "% Occupation"; Expression = {[math]::round(($_.Size-$_.FreeSpace) * 100 / $_.Size,2)}}
Get-WmiObject -Class Win32_LogicalDisk -Filter "DriveType=3" | format-table $fields | Out-String

"MISE A JOUR ?"
"-----------------"

$Isuptodate = "Votre OS utilise la dernière version connue"
$Isntuptodate = "Votre OS n'utilise pas la dernière version."
$updatecount = "Le nombre d'updates a faire sur l'OS est de:"
$updateObject = New-Object -ComObject Microsoft.Update.Session
$updateObject.ClientApplicationID = "Serverfault Example Script"
$updateSearcher = $updateObject.CreateUpdateSearcher()
$searchResults = $updateSearcher.Search("IsInstalled=0")
if ( $searchResults.Updates.Count -eq 0 ) {
Write-Host $Isuptodate
} else { 
Write-Host $Isntuptodate $updatecount $searchResults.Updates.Count
}


"TEST CONNECTION"
"------------"
ping 8.8.8.8
"USERS"
"-------------"
get-localuser | select Name